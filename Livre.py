class Livre:
    """
    Définition de la Façon de construire un objet « Livre »
    """

    def __init__(self, un_auteur, un_titre, un_editeur):
        """
        Définition de la méthode permettant de construire l'objet.
        Trois attributs définissent ce qu'est un livre
        """
        self.auteur = un_auteur
        self.titre = un_titre
        self.editeur = un_editeur

    def couverture(self):
        """
        Méthode affichant la couverture d'un livre.
        """
        print(self.titre)
        print("de", self.auteur)
        print("édité par", self.editeur)
