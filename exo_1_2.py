class Livre:
    """
    Définition de la Façon de construire un objet « Livre »
    """

    def __init__(self, un_auteur, un_titre, un_editeur, un_prix):
        """
        Définition de la méthode permettant de construire l'objet.
        Quatre attributs définissent ce qu'est un livre
        """
        self.auteur = un_auteur
        self.titre = un_titre
        self.editeur = un_editeur
        self.prix = un_prix

    def couverture(self):
        """
        Méthode affichant la couverture d'un livre.
        """
        print(self.titre)
        print("de", self.auteur)
        print("édité par", self.editeur)


if __name__ == "__main__":
    l1 = Livre("Michel Houellebecq", "Extension du domaine de la lutte",
               "Éditions Maurice Nadeau", 12.99)
    l2 = Livre("Bruno Clément-Petremann", "StrummerVille",
               "La Tengo Editions", 14.99)
    # l1.couverture()
    # l2.couverture()
    l3 = Livre("Tristan Nitot", "surveillance://", "C & F Éditions", 19)
    l3.couverture()
